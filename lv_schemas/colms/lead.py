from enum import Enum

from lv_schemas.base import BaseSchema


class IdType(str, Enum):
    SA_ID = "SA ID"
    PASSPORT = "Passport"
    DATE_OF_BIRTH = "DateOfBirth"


class LeadUpdate(BaseSchema):
    lead_id: str
    status: str
    status_reason: str
    reference: str


class LeadWrite(BaseSchema, kw_only=True):
    company: str
    full_name: str
    surname: str
    id_type: IdType
    id_no: str | None = None
    cell_number: str
    campaign: str | None = None
    lead_source: str | None = None
    product_name: str | None = None
    business_name: str | None = None
    language: str | None = None
    age: int | None = None
    title: str | None = None
    initials: str | None = None
    gender: str | None = None
    marital_status: str | None = None
    branch: str | None = None
    physical_address: str | None = None
    physical_province: str | None = None
    physical_city: str | None = None
    physical_suburb: str | None = None
    physical_postal_code: str | None = None
    postal_address: str | None = None
    postal_province: str | None = None
    postal_city: str | None = None
    postal_postal_code: str | None = None
    additional_contact_numbers: list[str]
    tel_work: str | None = None
    tel_home: str | None = None
    email: str | None = None
    category: str | None = None
    special_comments: str | None = None


class LeadBulkCreate(BaseSchema):
    external_ref: str
    data: list[LeadWrite]


class LeadRead(LeadWrite, kw_only=True):
    lead_id: str
    status: str
    status_reason: str | None = None
    reference: str | None = None
    age: str | None = None


class LeadWriteResponse(BaseSchema):
    result: bool
    lead_id: str
    message: str


class LeadReadResponse(BaseSchema):
    result: bool
    data: LeadRead


class LeadBulkReadResponse(BaseSchema):
    result: bool
    data: list[LeadRead]
