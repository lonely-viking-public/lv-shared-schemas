from datetime import datetime
from uuid import UUID

from lv_schemas.base import CamelCaseBaseSchema
from lv_schemas.finance.enums import BankAccountType


class BankDetailsCreate(CamelCaseBaseSchema):
    account_holder: str
    account_number: str
    bank_name: str
    account_type: BankAccountType


class BankDetailsUpdate(CamelCaseBaseSchema):
    account_holder: str
    account_number: str
    bank_name: str
    account_type: BankAccountType


class BankDetailsExternalRead(CamelCaseBaseSchema):
    account_holder: str
    account_number: str
    account_type: BankAccountType
    bank_name: str
    branch_code: str


class BankDetailsRead(BankDetailsExternalRead):
    id: UUID
    created_at: datetime
    created_by: str
    updated_at: datetime | None
    modified_by: str | None
