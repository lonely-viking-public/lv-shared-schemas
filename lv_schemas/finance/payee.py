from uuid import UUID

from lv_schemas.base import CamelCaseBaseSchema
from lv_schemas.finance.bank_details import BankDetailsCreate, BankDetailsRead
from lv_schemas.finance.enums import PayeeType


class PayeeBase(CamelCaseBaseSchema):
    payee_type: PayeeType
    name: str
    email: str | None
    telephone_number: str | None


class PayeeRead(PayeeBase):
    """Payee read schema."""

    id: UUID
    bank_details: BankDetailsRead


class PayeeWrite(PayeeBase, kw_only=True):
    """Payee creation/update schema."""

    id: UUID | None = None
    bank_details: BankDetailsCreate


class PayeeUpdate(PayeeBase):
    """Payee update schema."""
