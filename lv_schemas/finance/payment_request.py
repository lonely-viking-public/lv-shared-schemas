from datetime import datetime
from uuid import UUID

from lv_schemas.base import CamelCaseBaseSchema
from lv_schemas.finance.enums import BeneficiaryType, PaymentRequestStatus


class LineItemCreate(CamelCaseBaseSchema):
    """Line item creation schema."""

    amount: float
    description: str
    account_id: UUID
    department_id: UUID


class LineItemReadBasic(CamelCaseBaseSchema):
    id: UUID
    amount: float
    description: str
    account_name: str
    department_name: str
    department_id: UUID
    account_id: UUID
    context: str | None
    tax_type: str | None


class PaymentRequestCreate(CamelCaseBaseSchema):
    """Payment request creation schema."""

    beneficiary_type: BeneficiaryType
    beneficiary_id: UUID | None
    beneficiary_reference: str
    description: str
    project_reference: str
    invoice_number: str
    invoice_total: float
    line_items: list[LineItemCreate]


class PaymentRequestReadBasic(CamelCaseBaseSchema):
    id: UUID
    beneficiary_id: UUID | None
    beneficiary_type: BeneficiaryType
    beneficiary_name: str | None
    beneficiary_reference: str | None
    description: str
    project_reference: str
    invoice_number: str
    invoice_total: float
    status: PaymentRequestStatus
    number: int
    verified_at: datetime | None
    verified_by: str | None
    rejected_at: datetime | None
    rejected_by: str | None
    rejection_note: str | None
    created_by: str
    modified_by: str | None
    created_at: datetime
    updated_at: datetime | None
    organisation_id: UUID
    is_overridden: bool
    line_items: list[LineItemReadBasic]
