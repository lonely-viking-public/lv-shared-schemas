from enum import Enum


class BankAccountType(str, Enum):
    CHEQUE = "cheque"
    SAVINGS = "savings"
    CREDIT_CARD = "credit_card"


class BeneficiaryType(str, Enum):
    STAFF = "staff"
    SUPPLIER = "supplier"
    PAYEE = "payee"


class PayeeType(str, Enum):
    RECRUITER = "recruiter"
    VOLUNTEER = "volunteer"


class PaymentRequestStatus(str, Enum):
    APPROVED = "approved"
    DRAFT = "draft"
    EXPIRED = "expired"
    EXPORTED = "exported"
    FINALISED = "finalised"
    PARTIALLY_APPROVED = "partially_approved"
    REJECTED = "rejected"
    SUBMITTED = "submitted"
    OVERRIDDEN = "overridden"
