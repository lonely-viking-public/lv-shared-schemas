from collections.abc import Sequence
from typing import Any, Self

from msgspec import Struct, convert


class BaseSchema(Struct):
    """The base schema class."""

    @classmethod
    def from_attributes(cls, obj: Any) -> Self:
        return convert(obj, cls, from_attributes=True)

    @classmethod
    def from_objects(cls, objects: Sequence[Any]) -> list[Self]:
        return convert(objects, list[cls], from_attributes=True)

    def to_dict(self, exclude: set[str] | None = None) -> dict[str, Any]:
        exclude = exclude or []
        return {f: getattr(self, f) for f in self.__struct_fields__ if f not in exclude}


class CamelCaseBaseSchema(BaseSchema, rename="camel"):
    """Transforms keys to camelCase."""
