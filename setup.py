import setuptools

setuptools.setup(
    name="lv-shared-schemas",
    version="0.6.1",
    author="Michael Bosch",
    author_email="michael@lonelyviking.com",
    description="Common schemas shared among multiple applications.",
    packages=setuptools.find_packages(),
    classifiers=["Programming Language :: Python :: 3", "License :: MIT License", "Operating System :: OS Independent"],
    python_requires=">=3.10",
    install_requires=[
        "msgspec>=0.18.6",
    ],
)
